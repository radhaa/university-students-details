import Login from './components/login/login';
import StudentDetails from './components/studentsList/StudentDetails';
import EditComponent from './components/Edit/EditComponent'
import CreateComponent from './components/create/CreateComponent';
import ViewStudentDetails from './components/studentsList/ViewStudentDetails';
const routes = [
    {
        path: "/login",
        component: Login
    },
    {
        path: "/studentDetails",
        component: StudentDetails
    },

    {
        path: '/createStudent',
        component: CreateComponent

    },
    {
        path: '/viewStudent/:id',
        component: ViewStudentDetails

    }
    ,
    {
        path: '/editStudent/:id',
        component: EditComponent

    }
];

export default routes;