import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Header from '../header/header';
import Sidebar from '../sidebar/sidebar';

let drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
}));

function Layout(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);

    const handleDrawer = (action) => {
      setOpen(action);
      if(action){
        drawerWidth = 240;
      }
      else{
        drawerWidth = 0;
      }
    };
    return (
      <div>
        <Header {...props} handleDrawer={handleDrawer}/>
        <Sidebar {...props} drawerOpen={open}/>
        <div style={{marginLeft:240,marginTop:50}}>
          <main
            className={clsx(classes.content, {
              [classes.contentShift]: open,
            })}
          >
            {props.children}
          </main>
        </div>
      </div>
    );
  }
  
  export default Layout;