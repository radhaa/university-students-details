import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { useState, useEffect } from 'react'
import axios from 'axios';
import { Button } from '@material-ui/core';

export default function ViewStudentDetails(props) {
  const [data, setData] = useState({
    "addressCity": '',
    "branch": '',
    "email": '',
    "fatherName": '',
    "firstName": '',
    "id": '',
    "lastName": '',
    "mobile": '',
    "state": '',
    "rollNumber": ''
  }); useEffect(() => {
    const GetData = async () => {
      const result = await axios(`http://localhost:9005/student/getByid/${props.match.params.id}`)
      setData(result.data);
      console.log(result.data.firstName)
    }
    GetData();
  }, []);

  return (
    <React.Fragment>
      <Typography variant="h5"  gutterBottom>{data.firstName + ' '}
          Details
      </Typography>
      <Grid container spacing={4}>
        <Grid item xs={6} sm={6}><TextField  label="First name"  fullWidth autoComplete="fname" /></Grid>
        <Grid item xs={6} sm={6}><TextField  label="Last name" fullWidth value={data.lastName}/> </Grid>
        <Grid item xs={6}><TextField fullWidth name="rollNumber"  label="rollNumber" value={data.rollNumber}/></Grid>
        <Grid item xs={6}><TextField fullWidth label="email"  value={data.email}/> </Grid>
        <Grid item xs={6}> <TextField fullWidth label="mobile"  value={data.mobile}/></Grid>
        <Grid item xs={12} sm={6}> <TextField fullWidth  name="branch" label="branch" value={data.branch}/></Grid>
        <Grid item xs={6}> <TextField fullWidth label="city" value={data.addressCity}/></Grid>
        <Grid item xs={6}> <TextField  label="state" fullWidth value={data.state}/> </Grid>
        <Grid item xs={6} sm={6}>  <TextField label="fatherName" fullWidth   value={data.fatherName}/></Grid>
        <Grid item xs={6} sm={6}><Button variant="contained" color="primary" onClick={() => { props.history.push('/studentDetails') }}>back</Button>        </Grid>
        </Grid>
    </React.Fragment>
  );
}