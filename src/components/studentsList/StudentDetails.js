import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import { useState, useEffect } from 'react'
import { Grid } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import VisibilityIcon from '@material-ui/icons/Visibility';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  }, grow: {
    flexGrow: 1,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: 0,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
    container: {
      maxHeight: 440,
    }
  }
}));
export default function StudentDetails(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [data, setData] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [searchValue, setSearchValue] = useState('');
  useEffect(() => {
    const GetData = async () => {
      const result = await axios('http://localhost:9005/student/getStudentDetails');
      setData(result.data);
    }
    GetData();
  }, []);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const serchByValue = async (e) => {
    console.log('some', e.keyCode)
    console.log(searchValue)
    if (e.keyCode === 13) {
      const result = await axios(`http://localhost:9005/student/searchByValue/${searchValue}`);
      setData(result.data);
    }
  }
  const deleteStudent = async (id) => {
    axios.delete(`http://localhost:9005/student/deleteByid/${id}`)
    const result = await axios('http://localhost:9005/student/getStudentDetails');
    setData(result.data);

  }
  const clear = async () => {
    const result = await axios('http://localhost:9005/student/getStudentDetails');
    setSearchValue('');
    setData(result.data);
  }
  return (
    <Paper className={classes.root}>
      <Grid container >
        <Grid item xs={4} sm={4}><h2>Student Details table </h2></Grid><Grid item xs={3} sm={3}></Grid><Grid item xs={3} sm={3}>
          <Toolbar>
            <Tooltip title="search">
              <IconButton aria-label="search">
                <SearchIcon onClick={() => { serchByValue(searchValue) }} />
              </IconButton>
            </Tooltip>
            <InputBase value={searchValue} onChange={e => { setSearchValue(e.target.value) }} onKeyUp={(e) => { serchByValue(e, searchValue) }}
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }} />
            <Tooltip title="clear filter">
              <IconButton aria-label="clear filter">
                <ClearIcon onClick={e => { clear() }} />
              </IconButton>
            </Tooltip>
          </Toolbar>
        </Grid>
        <Grid item xs={1} sm={1}></Grid>
        <Grid item xs={1} sm={1}> <Tooltip title="Add" aria-label="add">
          <Fab color="primary" size="medium" className={classes.fab}>
            <AddIcon onClick={() => props.history.push('/createStudent')} />
          </Fab>
        </Tooltip>
        </Grid>
      </Grid>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell align="left">rollNumber</TableCell>
              <TableCell align="left">firstName</TableCell>
              <TableCell align="left">lastName</TableCell>
              <TableCell align="left">email</TableCell>
              <TableCell style={{ paddingRight: "60px" }} align="left" >branch</TableCell>
              <TableCell align="left">mobileNumber</TableCell>
              <TableCell style={{ minWidth: 150 }} align="left">actions.</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
              return (
                <TableRow >
                  <TableCell component="th" scope="row">
                    {row.id}
                  </TableCell>
                  <TableCell align="left">{row.rollNumber}</TableCell>
                  <TableCell align="left">{row.firstName}</TableCell>
                  <TableCell align="left">{row.lastName}</TableCell>
                  <TableCell align="left">{row.email}</TableCell>
                  <TableCell style={{ paddingRight: "60px" }} align="left">{row.branch}</TableCell>
                  <TableCell align="left">{row.mobile}</TableCell>
                  <TableCell style={{ minWidth: '100px' }} align="left" >
                    <Tooltip title="Delete" onClick={e => { deleteStudent(row.id) }}>
                      <IconButton aria-label="delete">
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip><Tooltip title="Edit" onClick={() => props.history.push(`/editStudent/${row.id}`)}>
                      <IconButton aria-label="Edit">
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="view" onClick={() => props.history.push(`/viewStudent/${row.id}`)}>
                      <IconButton aria-label="view">
                        <VisibilityIcon />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 15]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
} 