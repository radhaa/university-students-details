import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useState, useEffect } from 'react'
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    submit: {
        margin: theme.spacing(1, 0, 2),
    },
}));

export default function EditComponent(props) {
    const classes = useStyles();

    const [data, setData] = useState({
        "addressCity": '',
        "branch": '',
        "email": '',
        "fatherName": '',
        "firstName": '',
        "id": '',
        "lastName": '',
        "mobile": '',
        "state": '',
        "rollNumber": ''
    });
    useEffect(() => {
        const GetData = async () => {
            const result = await axios(`http://localhost:9005/student/getByid/${props.match.params.id}`)
            setData(result.data); setData(result.data);
        }
        GetData();
    }, []
    );
    const editData = (data) => {
        console.log(data)
        axios.put("http://localhost:9005/student/updateStudent", {
            "addressCity": data.addressCity,
            "branch": data.branch,
            "email": data.email,
            "fatherName": data.fatherName,
            "firstName": data.firstName,
            "id": data.id,
            "lastName": data.lastName,
            "mobile": data.mobile,
            "state": data.state,
            "rollNumber": data.rollNumber
        })
        props.history.push('/studentDetails');
    }
    const changeFunction = (nameOfFeild, value) => {
        let data1 = data;
        switch (nameOfFeild) {
            case "firstName":
                data1.firstName = value;
                break;
            case "lastName":
                data1.lastName = value;
                break;
            case "email":
                data1.email = value;
                break;
            case "branch":
                data1.branch = value;
                break;
            case "mobile":
                data1.mobile = value;
                break;
            case "state":
                data1.state = value
                break;
            case "addressCity":
                data1.addressCity = value;
                break;
            default:
                break;
        }
        setData({ ...data1 });
    }


    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Grid container spacing={1}>
                    <Grid item xs={12} sm={6}><TextField autoComplete="fname" name="firstName" variant="outlined" required fullWidth id="firstName" label="First Name" onChange={e => changeFunction("firstName", e.target.value)} value={data.firstName} autoFocus /></Grid>
                    <Grid item xs={12} sm={6}> <TextField variant="outlined" required fullWidth id="lastName" label="Last Name" value={data.lastName} onChange={e => changeFunction("lastName", e.target.value)} /></Grid>
                    <Grid item xs={12} sm={12}> <TextField variant="outlined" required fullWidth id="email" label="Email Address" value={data.email} onChange={e => changeFunction("email", e.target.value)} name="email" /></Grid>
                    <Grid item xs={12} sm={12}>  <TextField variant="outlined" required fullWidth id="mobile" label="mobile number" value={data.mobile} name="mobile" onChange={e => changeFunction("mobile", e.target.value)} autoComplete="mobile" /></Grid>
                    <Grid item xs={12} sm={12}> <TextField variant="outlined" required fullWidth id="branch" label="branch" name="branch" value={data.branch} onChange={e => changeFunction("branch", e.target.value)} autoComplete="branch" /></Grid>
                    <Grid item xs={6} sm={6}>  <TextField variant="outlined" required fullWidth id="city" label="Address city" name="city" value={data.addressCity} onChange={e => changeFunction("addressCity", e.target.value)} autoComplete="city" /></Grid>
                    <Grid item xs={6} sm={6}> <TextField variant="outlined" required fullWidth id="state" label="Address state" name="state" value={data.state} onChange={e => changeFunction("state", e.target.value)} autoComplete="state" /></Grid>
                    <Grid item xs={12} sm={12}> <TextField variant="outlined" required fullWidth id="rollNumber" label="roll number" name="rollNumber" readOnly autoComplete="rollNumber" value={data.rollNumber} /></Grid>
                    <Grid item xs={12} sm={12}>   <TextField variant="outlined" margin="normal" required fullWidth name="fatherName" label="father name" type="txt" value={data.fatherName} readOnly id="fatherName" autoComplete="current-password" />  </Grid>
                    <Grid item xs={12} sm={6}>  <Button type="submit" variant="contained" color="secondary" className={classes.submit} onClick={() => props.history.push(`/studentDetails`)}>cancel</Button>  </Grid>
                    <Grid item xs={12} sm={6}>   <Button type="submit" variant="contained" color="primary" className={classes.submit} onClick={e => { editData(data) }}>Edit</Button> </Grid>
                </Grid>

            </div>
        </Container>
    );

}
