import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';

let drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  }
}));

export default function Sidebar(props) {
  const classes = useStyles();
  props.drawerOpen ? drawerWidth = 240 : drawerWidth = 0;
  return (
    <Drawer
      className={classes.drawer}
      style={{ width: drawerWidth }}
      variant="persistent"
      anchor="left"
      open={props.drawerOpen}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
      </div>
      <Divider />
      <List>
        <ListItem button key={1} onClick={() => props.history.push('/studentDetails')}>
          <ListItemIcon><InboxIcon /></ListItemIcon>
          <ListItemText primary={'studentDetails'} />
        </ListItem>
      </List>
    </Drawer>
  )
}