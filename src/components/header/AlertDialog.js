import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

export default function AlertDialog(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    console.log(value)
    console.log(props)
    if (value === 'agree') {
      props.history.push('/login')
    }
    setOpen(false);
  };

  return (
    <div>
      <IconButton position='marginRight' marginRight='auto' color="inherit" onClick={handleClickOpen} edge="end" > <ExitToAppIcon />  </IconButton>
      <Dialog open={open} onClose={handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description"  >
        <DialogTitle id="alert-dialog-title">{"Are you sure, do you want to log out."}</DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">no </Button>
          <Button onClick={() => { handleClose('agree') }} color="primary" autoFocus> yes </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
