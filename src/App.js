import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Layout from './components/layout/layout';
import Login from './components/login/login';
import routes from './route';

function App() {
  return (
    <div className="App">
        <Router>
          <Switch>
            <Route path={'/login'} component={Login}/>
            <Route path={'/'} render={props => { 
              return (
                <Layout {...props}>
                  <Switch>
                    {routes.map((route) => (
                      <Route
                        key={route.path}
                        path={route.path}
                        component={route.component}
                      />
                    ))}
                    <Redirect from="/" to="/login"/>
                  </Switch>
                </Layout>
              )
            }}/>
          </Switch>
        </Router>
    </div>
  );
}

export default App;
